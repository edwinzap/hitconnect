/**
 * Created by forge on 26/12/2017.
 */
function showContact(id){
    var showContactForm = document.getElementById('show_contact_form');
    showContactForm.elements.namedItem("id").value= id;

    var form = $("#show_contact_form");
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function ( data) {
            $("#profil-content").html(data);
        },
        error: function (data) {
            console.log('error');
        }});
}

function acceptConnection(id){
    var form = document.getElementById('accept_connection_form');
    form.elements.namedItem("id").value = id;
    form.submit();
}

function refuseConnection(id){
    var form = document.getElementById('remove_connection_form');
    form.elements.namedItem("id").value = id;
    form.submit();
}

function askConnection(id, userid){
    var form = document.getElementById('ask_connection_form');
    form.elements.namedItem("id").value = id;
    form.elements.namedItem("userid").value = userid;
    form.submit();
}

function removeConnection(id){
    var form = document.getElementById('remove_connection_form');
    form.elements.namedItem("id").value = id;
    form.submit();
}
