/**
 * Created by forge on 25/11/2017.
 */


/*window.onload = function () {
    var table = document.getElementsByTagName('table');
    for (var i = 0; i< table.length; i++){
        table[i].onclick = highlight;
    }

    function highlight(e) {
        var target = e.target.parentNode;
        if (target.className == 'selected'){
            target.className = '';
        }
        else{
            target.className = 'selected';
        }

    }
};*/

function addRecording(e){
    var row = e.parentNode.parentNode;
    var artistId = row.getElementsByClassName("artistid")[0].innerHTML;
    var artistName = row.getElementsByClassName("artistname")[0].innerHTML;
    var recordingId = row.getElementsByClassName("recordingid")[0].innerHTML;
    var recordingName = row.getElementsByClassName("recordingname")[0].innerHTML;


    var addForm = document.getElementById('add_preferences_form');

    addForm.elements.namedItem("artistid").value = artistId;
    addForm.elements.namedItem("artistname").value=artistName;
    addForm.elements.namedItem("recordingid").value=recordingId;
    addForm.elements.namedItem("recordingname").value= recordingName;

    var form = $("#add_preferences_form");

    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function ( data) {
            $("#table2_body").html(data);
        },
        error: function (data) {
            console.log('error');
        }});
}

function removeRecording(e){
    var row = e.parentNode.parentNode;
    var uptId = row.getElementsByClassName("uptid")[0].innerHTML;

    var removeForm = document.getElementById('remove_preferences_form');
    removeForm.elements.namedItem("uptid").value= uptId;

    var form = $("#remove_preferences_form");
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function ( data) {
            $("#table2_body").html(data);
        },
        error: function (data) {
            console.log('error');
        }});
}

$(document).on('submit', '#search_form', function(event) {
    var title =document.getElementById('search_title').value;
    var artist = document.getElementById('search_artist').value;
    if(title != '' || artist != ''){
        var form = $(this);
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function ( data) {
                $("#table1_body").html(data);
            },
            error: function ( data ) {
                console.log('error');
            }});
    }
    event.preventDefault(); // Prevent the form from submitting via the browser.
});

