<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 25/11/2017
 * Time: 14:27
 */
return [

    'input' => '<input type="{{type}}" name="{{name}}" class="form-control" {{attrs}} />',
    'dateWidget'=> '<div class="form-group form-inline required">{{day}}{{month}}{{year}}</div>',

    'inputContainer' => '<div class="form-group {{divclass}} {{required}}">{{content}}</div>',
    'inputContainerError' => '<div class="form-group has-error has-feedback {{required}}">{{content}}{{error}}<span class="glyphicon glyphicon-remove form-control-feedback"></span></div>',
    'error' => '<div class="help-block">{{content}}</div>',
];
