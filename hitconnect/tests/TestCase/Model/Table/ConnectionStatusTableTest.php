<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConnectionStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConnectionStatusTable Test Case
 */
class ConnectionStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConnectionStatusTable
     */
    public $ConnectionStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.connection_status',
        'app.connections',
        'app.users',
        'app.user_preferred_titles',
        'app.recordings',
        'app.artists',
        'app.tags',
        'app.artists_tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConnectionStatus') ? [] : ['className' => ConnectionStatusTable::class];
        $this->ConnectionStatus = TableRegistry::get('ConnectionStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConnectionStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
