<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UserPreferredTitlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UserPreferredTitlesTable Test Case
 */
class UserPreferredTitlesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UserPreferredTitlesTable
     */
    public $UserPreferredTitles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.user_preferred_titles',
        'app.users',
        'app.recordings',
        'app.artists',
        'app.tags',
        'app.artists_tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UserPreferredTitles') ? [] : ['className' => UserPreferredTitlesTable::class];
        $this->UserPreferredTitles = TableRegistry::get('UserPreferredTitles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UserPreferredTitles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
