<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConnectionsFixture
 *
 */
class ConnectionsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => 'CURRENT_TIMESTAMP', 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'source_user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'target_user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'connection_status_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'connections_source_users_fk' => ['type' => 'index', 'columns' => ['source_user_id'], 'length' => []],
            'connections_target_users_fk' => ['type' => 'index', 'columns' => ['target_user_id'], 'length' => []],
            'connections_connection_status_fk' => ['type' => 'index', 'columns' => ['connection_status_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'connections_connection_status_fk' => ['type' => 'foreign', 'columns' => ['connection_status_id'], 'references' => ['connection_status', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'connections_source_users_fk' => ['type' => 'foreign', 'columns' => ['source_user_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'connections_target_users_fk' => ['type' => 'foreign', 'columns' => ['target_user_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'created' => '2017-12-25 11:44:18',
            'modified' => '2017-12-25 11:44:18',
            'source_user_id' => 1,
            'target_user_id' => 1,
            'connection_status_id' => 1
        ],
    ];
}
