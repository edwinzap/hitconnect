<?php $this->assign('title', 'Accueil') ?>

<?php $this->start('header')?>
<h1>HIT-CONNECTS</h1>
<?php $this->end()?>

<div class="container text-center">
    <h3>Bienvenue sur Hit-Connects, le réseau social musical par excellence.</h3>
    <h3>Rejoignez-nous pour vivre une aventure musicale hors du commun !</h3>
    <p class="btn-inline">
        <a href="/users/add" class="btn btn-action btn-lg" role="button">S'inscrire</a>
        <a href="/users/login" class="btn btn-success btn-lg" role="button">Se connecter</a>
    </p>
    <?=$this->Html->image('music-background.jpg', ['alt'=>'music background', 'id'=>'music-background'])?>
</div>
