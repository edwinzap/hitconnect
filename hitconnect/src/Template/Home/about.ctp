<?php $this->assign('title', 'A propos') ?>

<?php $this->start('header')?>
<h1>A propos</h1>
<?php $this->end()?>

<div class="container">
    <h3>Auteur</h3>
    <p>Miguel FORGET est actuellement étudiant en dernière année d'informatique de gestion à l'ESA (Ecole Supérieure des Affaires de Namur)</p>
    <h3>Projet</h3>
    <p>Un nouveau réseau social voit le jour: «Hit-Connects», afin de permettre à des internautes d’établir des «connexions» avec des membres qui partagent des goûts musicaux semblables.<p>
    <p>Voici les fonctionnalités de la plateforme: </p>
    <ul class="list-square">
        <li>Tout visiteur doit pouvoir s’enregistrer et se créer un profil (nom, prénom, adresse email)</li>
        <li>Tout visiteur connecté doit pouvoir indiquer les morceaux musicaux qu’il préfère, sur base de recherches qu’il aura réalisées dans un catalogue accessible via des web services en ligne</li>
        <li>Une fois le profil d’un utilisateur complété avec ses préférences musicales, la plateforme doit lui proposer d’ajouter des «connexions»,
            c’est-à-dire des utilisateurs qui ont des préférences musicales similaires</li>
        <li>Si l’utilisateur «accepte» une connexion proposée par la plateforme,
            une invitation est envoyée au membre correspondant. Lorsque les deux utilisateurs acceptent,
            ils sont alors «connectés» et peuvent consulter leur profil complet (avec l’email) et ainsi se contacter directement.</li>
    </ul>
    <h3>Professeurs</h3>
    <p>Mr. Frédéric PARION, Mr. Frédéric FILEE</p>
</div>
