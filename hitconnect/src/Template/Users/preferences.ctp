<?php $this->assign('title', 'Préférences musicales') ?>
<?= $this->Html->script('table', ['block' => 'script']) ?>
<?php $this->start('header')?>
<h1>Préférences musicales</h1>
<?php $this->end()?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="container">
            <div class="text-center">
                <?=$this->Flash->render()?>
            </div>
            <?=$this->Form->create(null, ['id'=>'search_form', 'novalidate'=>true])?>
            <div class="form-row">
                <?= $this->Form->control('search_title', ['id'=>'search_title','type'=>'text', 'templateVars'=>['divclass'=>'col-sm-4'], 'label'=>'Titre']) ?>
                <?= $this->Form->control('search_artist', ['id'=>'search_artist','type'=>'text', 'templateVars'=>['divclass'=>'col-sm-4'], 'label'=>'Artist']) ?>
                <div class="row col-sm-4">
                    <div class="col-xs-6">
                        <?= $this->Form->submit(__('OK'),['class'=>'btn btn-danger']);?>
                    </div>
                    <div class="col-xs-6">
                        <?= $this->Html->link('Terminé', '/users/index', ['class'=>'btn btn-action'])?>
                    </div>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
    </div>

    <div class="hidden">
        <?=$this->Form->create(null, ['id'=>'add_preferences_form', 'url'=>['action'=>'addRecording']])?>
        <?=$this->Form->input('recordingid')?>
        <?=$this->Form->input('recordingname')?>
        <?=$this->Form->input('artistid')?>
        <?=$this->Form->input('artistname')?>
        <?=$this->Form->end()?>
    </div>

    <div class="hidden">
        <?=$this->Form->create(null, ['id'=>'remove_preferences_form', 'url'=>['action'=>'removeRecording']])?>
        <?=$this->Form->input('uptid')?>
        <?=$this->Form->end()?>
    </div>

    <div class="panel-body">
        <div class="row container-fluid">
            <div class="col-sm-6">
                <h4>Résultat de la recherche</h4>
                <table id="table1" class="table table-bordered table-striped">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Artiste</th>
                        <th>Titre</th>
                    </tr>
                    </thead>
                    <tbody  id="table1_body">
                    </tbody>
                </table>
            </div>

            <div class="col-sm-6">
                <h4>Mes titres préférés</h4>
                <table id="table2" class="table table-bordered table-striped">
                    <thead class="thead-inverse">
                    <tr>
                        <th hidden>Id</th>
                        <th>Artiste</th>
                        <th>Titre</th>
                    </tr>
                    </thead>
                    <tbody id="table2_body">
                    <?= $this->element('upts')?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



