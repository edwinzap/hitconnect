<?php $this->assign('title', 'Mon Profil') ?>
<?= $this->Html->script('profile', ['block' => 'script']) ?>

<?php $this->start('header')?>
<h1>Mon Profil</h1>
<?php $this->end()?>

<!--Forms Hidden-->
<div class="hidden">
    <?= $this->Form->create(null, ['id'=>'show_contact_form','url'=>['action'=>'getContact'], 'novalidate'=>true])?>
    <?= $this->Form->control('id') ?>
    <?= $this->Form->end()?>

    <?= $this->Form->create(null, ['id'=>'accept_connection_form','url'=>['action'=>'acceptContact'], 'novalidate'=>true])?>
    <?= $this->Form->control('id') ?>
    <?= $this->Form->end()?>

    <?= $this->Form->create(null, ['id'=>'remove_connection_form','url'=>['action'=>'removeContact'], 'novalidate'=>true])?>
    <?= $this->Form->control('id') ?>
    <?= $this->Form->end()?>

    <?= $this->Form->create(null, ['id'=>'ask_connection_form','url'=>['action'=>'askContact'], 'novalidate'=>true])?>
    <?= $this->Form->control('id') ?>
    <?= $this->Form->control('userid') ?>
    <?= $this->Form->end()?>
</div>

<!--<button class="visible-xs btn btn-info" data-toggle="collapse" data-target="#left-panel">Contacts</button>
<button class="visible-xs btn btn-info" data-toggle="collapse" data-target="#right-panel">Notifications</button>-->

<div class="flex-page">
    <div id="left-panel" class="vertical-menu vertical-menu-left col-xs-12 col-md-4 col-lg-3">
        <div>
            <h3 class="text-center">Mes contacts</h3>
            <div class="list-group" id="contacts">
                <?php foreach($contacts as $contact): ?>
                <?php $id=$contact['userid']?>
                <div class="list-group-item" onclick="showContact('<?= $id ?>')"><?= $contact['firstname']?> <?= $contact['lastname']?></div>
                <?php endforeach ?>
            </div>
        </div>
    </div>

    <div id="profil-content">
        <?= $this->element('profile')?>
    </div>

    <div id="right-panel" class="vertical-menu vertical-menu-right col-xs-12 col-md-4 col-lg-3">
        <div>
            <div id="contact-suggestion-bar">
                <h3 class="text-center">Invitations</h3>
                <div class="list-group">
                    <?php foreach($invitations as $invitation): ?>
                    <?php $id=$invitation['connectionid'] ?>
                    <div class="list-group-item">
                        <?= $invitation['firstname']?> <?= $invitation['lastname']?>
                        <div class="pull-right">
                            <span class="glyphicon glyphicon-ok-sign text-success"  onclick="acceptConnection('<?= $id ?>')"></span>
                            <span class="glyphicon glyphicon-remove-sign text-danger"  onclick="refuseConnection('<?= $id ?>')"></span>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
            <div id="contact-match-bar">
                <h3 class="text-center">Suggestions</h3>
                <div class="list-group">
                    <?php foreach($matches as $match): ?>
                    <?php $id=$match['connectionid']; $userid=$match['userid']?>
                    <div class="list-group-item">
                        <?= $match['firstname']?> <?= $match['lastname']?>
                        <div class="pull-right">
                            <span class="glyphicon glyphicon-plus-sign text-success"  onclick="askConnection('<?= $id ?>','<?= $userid ?>')"></span>
                            <span class="glyphicon glyphicon-remove-sign text-danger"  onclick="refuseConnection('<?= $id ?>')"></span>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div id="contact--bar">
                <h3 class="text-center">Demandes en attente</h3>
                <div class="list-group">
                    <?php foreach($demands as $demand): ?>
                    <?php $id=$demand['connectionid'] ?>
                    <div class="list-group-item">
                        <?= $demand['firstname']?> <?= $demand['lastname']?>
                        <div class="pull-right">
                            <span class="glyphicon glyphicon-remove-sign text-danger"  onclick="removeConnection('<?= $id ?>')"></span>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>

