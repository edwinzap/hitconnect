<?php $this->assign('title', 'Connexion') ?>

<?php $this->start('header')?>
<h1>Connexion</h1>
<?php $this->end()?>
<div class="container col-xs-12 col-sm-8 col-md-6 col-lg-5">
    <div class="top-margin">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $this->Flash->render()?>
                <?= $this->Form->create(); ?>
                <?= $this->Form->control('email', ['type'=>'email', 'required'=>true, 'label'=>'Email']) ?>
                <?= $this->Form->control('password', ['type'=>'password', 'required'=>true, 'label'=>'Mot de passe']) ?>
                <hr>
                <div class="row">
                    <div class="col-lg-6">
                        <p><strong><?= $this->Html->link('Mot de passe oublié','#')?></strong></p>
                        <p><strong><?= $this->Html->link('Créer un compte','/users/add')?></strong></p>
                    </div>
                    <div class="col-lg-6 text-right">
                        <?= $this->Form->button(__('Se connecter'), ['class'=>'btn btn-action']); ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
