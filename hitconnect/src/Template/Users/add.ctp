<?php $this->assign('title', 'Inscription') ?>
<?php $this->start('header')?>
<h1>Inscription</h1>
<?php $this->end()?>
<div class="container col-xs-12 col-sm-8 col-md-6 col-lg-5">
    <div class="top-margin">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $this->Flash->render()?>

                <?= $this->Form->create($user)?>
                <?= $this->Form->control('lastname', ['type'=>'text', 'required'=>true, 'label'=>'Nom']) ?>
                <?= $this->Form->control('firstname', ['type'=>'text', 'required'=>true, 'label'=>'Prénom']) ?>
                <?= $this->Form->control('birthday', ['type'=>'date', 'label'=>'Date de naissance',
                'minYear'=>date('Y')-70, 'maxYear'=>date('Y'),
                'empty' => ['day'=>'Jour','month'=>'Mois','year'=>'Année'],
                'day'=>['class'=>'form-control'],
                'month'=>['class'=>'form-control'],
                'year'=>['class'=>'form-control']
                ]) ?>
                <?= $this->Form->control('email', ['type'=>'email', 'required'=>true, 'label'=>'Email']) ?>
                <?= $this->Form->control('password', ['type'=>'password', 'required'=>true, 'label'=>'Mot de passe']) ?>
                <?= $this->Form->control('confirm_password', ['type'=>'password', 'required'=>true, 'label'=>'Confirmation du mot de passe'])?>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="pull-left">
                            <?= $this->Html->link('Annuler','/home/index', ['class'=>'btn btn-info'])?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="pull-right">
                            <?= $this->Form->button(__('Terminer'), ['class'=>'btn btn-action']); ?>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end()?>
            </div>
        </div>
    </div>
</div>
