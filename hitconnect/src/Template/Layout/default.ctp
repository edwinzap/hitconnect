<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<div id="main-wrapper">
    <head>
        <title>
            <?= $this->fetch('title') ?>
        </title>

        <?= $this->Html->css('bootstrap.min') ?>
        <?= $this->Html->css('bootstrap-theme') ?>
        <?= $this->Html->css('main') ?>
        <?= $this->Html->css('custom') ?>

        <?= $this->Html->script('jquery-3.2.1.min') ?>
        <?= $this->Html->script('template') ?>
        <?= $this->Html->script('headroom.min') ?>
        <?= $this->Html->script('jQuery.headroom.min') ?>
        <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js') ?>
        <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js') ?>
        <?= $this->fetch('script') ?>
        <?= $this->fetch('css') ?>

    </head>
    <body>
    <!--Header avec menu-->
    <div class="navbar-inverse navbar-fixed-top headroom" id="header-container">
        <div class="container" >
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-header">
                <a class="navbar-brand" href="/home/index">
                    <?= $this->Html->image('favicon.png', ['alt'=>'HitConnect icon', 'class'=>'brand-image']) ?>
                    Hit-Connects
                </a>
            </div>
            <div class="navbar-collapse collapse" id="navbar-collapse">
                <ul class="nav navbar-nav pull-right">
                    <?= $this->element('menu', ['activeItem'=> $activeItem])?>
                </ul>
            </div>
        </div>
    </div>
    <header id="head">
        <div class="container top-space">
            <div class="row">
                <?= $this->fetch('header')?>
            </div>
        </div>
    </header>

    <!--Content-->
    <div id="body-container">
        <?= $this->fetch('content')?>
    </div>
    </body>
    <!--Footer-->
    <footer id="footer">
        <div class="footer-container footer2">
            <div class="container">
                <div class="row">

                    <div class="col-md-6 widget">
                        <div class="widget-body">
                            <ul class="simplenav">
                                <?= $this->element('menu_footer')?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6 widget">
                        <div class="widget-body">
                            <p class="text-right">
                                Copyright © 2017, Miguel FORGET. Designed by <a href="http://gettemplate.com/" rel="designer">gettemplate</a>
                            </p>
                        </div>
                    </div>

                </div> <!-- /row of widgets -->
            </div>
        </div>
    </footer>
</div>
</html>
