<div class="panel panel-default">
    <div class="panel-heading">
        <h2><?= $user->firstname?> <?= $user->lastname?></h2>
    </div>
    <div class="panel-body">
        <table>
            <tr>
                <td class="property-text">Date de naissance:</td>
                <td class="property-value"><?=$user->birthday?></td>
            </tr>
            <tr>
                <td class="property-text">Email:</td>
                <td class="property-value"><?=$this->Html->link($user->email, 'mailto:'.$user->email)?></td>
            </tr>
        </table>
        <div class="top-margin">
            <fieldset>
                <legend>Préférences musicales</legend>
                <table id="profile-table" class="table table-bordered table-striped">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Artiste</th>
                        <th>Titre</th>
                    </tr>
                    </thead>
                    <tbody id="profile_table_body">
                    <?php foreach($upts as $title): ?>
                    <tr>
                        <td class="artistname"><?=$title->recording->artist->label?></td>
                        <td class="recordingname"><?=$title->recording->label?></td>
                    </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>
</div>

