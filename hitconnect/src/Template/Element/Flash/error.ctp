<div class="message error alert alert-danger" onclick="this.classList.add('hidden');">
    <strong><?= $message ?></strong>
</div>
