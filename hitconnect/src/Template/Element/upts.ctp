<?php foreach($upts as $title): ?>
<tr>
    <td class="hidden uptid"><?=$title->id?></td>
    <td class="artistname"><?=$title->recording->artist->label?></td>
    <td class="recordingname"><?=$title->recording->label?></td>
    <td><span class="text-center center-block text-danger glyphicon glyphicon-remove" onclick="removeRecording(this)"></span></td>
</tr>
<?php endforeach ?>
