
<?php foreach($titles as $title): ?>
<tr>
    <td class="hidden recordingid"><?=$title->mbid?></td>
    <td class="hidden artistid"><?=$title->artist->mbid?></td>
    <td class="artistname"><?=$title->artist->label?></td>
    <td class="recordingname"><?=$title->label?></td>
    <td><span class="text-center center-block text-success glyphicon glyphicon-plus" onclick="addRecording(this)"></span></td>
</tr>
<?php endforeach ?>
