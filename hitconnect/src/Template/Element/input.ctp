<div class="form-group">
    <?php if(isset($required) && $required==true):?>
    <label><?=$label?><span class="text-danger">*</span></label>
    <input class="form-control" type="<?=$type?>" name="<?=$name?>" required>
    <?php else:?>
    <label><?=$label?></label>
    <input class="form-control" type="<?=$type?>" name="<?=$name?>">
    <?php endif ?>
</div>
