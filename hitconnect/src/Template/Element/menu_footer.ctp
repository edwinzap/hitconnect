
<?php if(!$auth_user):?>
<p class="simplenav">
    <?= $this->Html->link('Accueil','/home/index')?>
    |
    <?= $this->Html->link('A propos','/home/about')?>
    |
    <strong><?= $this->element('auth')?></strong>
</p>
<?php else: ?>
<p class="simplenav">
    <?= $this->Html->link($auth_user['firstname'],'/users/index')?>
    |
    <?= $this->Html->link('Préférences','/users/preferences')?>
    |
    <strong><?= $this->element('auth')?></strong>
</p>
<?php endif ?>
