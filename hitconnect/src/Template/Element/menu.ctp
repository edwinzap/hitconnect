<?php if(!$auth_user):?>
<li class="<?php echo ($activeItem=='home')?'active':''?>" >
    <?= $this->Html->link('Accueil','/home/index')?>
</li>
<li class="<?php echo ($activeItem=='about')?'active':''?>">
    <?= $this->Html->link('A propos','/home/about')?>
</li>
<li>
<?= $this->element('auth', ['class'=>'btn'])?>
</li>
<?php else: ?>
<li class="<?php echo ($activeItem=='profile')?'active':''?>" >
    <?= $this->Html->link($auth_user['firstname'],'/users/index')?>
</li>
<li class="<?php echo ($activeItem=='preferences')?'active':''?>">
    <?= $this->Html->link('Préférences','/users/preferences')?>
</li>
<li>
    <?= $this->element('auth', ['class'=>'btn'])?>
</li>
<?php endif ?>
