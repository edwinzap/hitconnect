<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 16/12/2017
 * Time: 15:24
 */

namespace App\Model\BU;


use Cake\Error\Debugger;
use Cake\Log\Log;
use MusicBrainz\Entities\Artist;
use MusicBrainz\Entities\Recording;
use MusicBrainz\Entities\Tag;

class Mapper
{
    public static function mapRecording(Recording $recording){
        $r = new \App\Model\Entity\Recording();
        $r->artist_id=$recording->getArtist()->getId();
        $r->mbid=$recording->getId();
        $r->label=$recording->getTitle();

        $artist = Mapper::mapArtist($recording->getArtist());
        $r->artist_id=$artist->id;
        $r->artist=$artist;

        return $r;
    }

    public static function mapArtist(Artist $artist){
        $a= new \App\Model\Entity\Artist();
        $a->mbid = $artist->getId();
        $a->label = $artist->getName();

        $tags = [];
        $tagsArray = $artist->getTags();
        if(count($tagsArray)>0){
            foreach ($tagsArray as $item){
                $tags[]=self::mapTag($item);
            }
        }
        $a->tags = $tags;
        return $a;
    }

    public static function mapTag(Tag $tag){
        $t = new \App\Model\Entity\Tag();
        $t->label = $tag->getName();
        return $t;
    }
}
