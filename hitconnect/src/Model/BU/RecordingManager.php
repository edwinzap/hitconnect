<?php
namespace App\Model\BU;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use MusicBrainz\Entities\Artist;
use MusicBrainz\Entities\Recording;
use MusicBrainz\MusicBrainzServiceAgent;

/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 03/12/2017
 * Time: 15:24
 */
class RecordingManager
{
    /**
     * Récupère une liste de Recording venant de Music Brainz
     * @param $title
     * @return array de Recordings
     */
    private static function searchTitleMb($title){
        $mb = new MusicBrainzServiceAgent();
        $recordings= $mb->Recording()->search($title);
        return self::mapRecordings($recordings);
    }

    private static function searchArtistMb($artist){
        $mb = new MusicBrainzServiceAgent();
        $query = sprintf("artist:%s", $artist);
        $recordings = $mb->Recording()->search($query);
        return self::mapRecordings($recordings);
    }

    private static function searchTitleAndArtistMb($title, $artist){
        $mb = new MusicBrainzServiceAgent();
        $query = sprintf('recording:%s AND artist:%s', $title,$artist);
        $recordings = $mb->Recording()->search($query);
        return self::mapRecordings($recordings);
    }

    public static function searchMb($title, $artist=null){
        $title= str_replace('.','',$title);
        $artist= str_replace('.','',$artist);

        if(!empty($title) || !empty($artist)){
            if(empty($artist)){
                Log::debug("Recherche du titre: ". $title);
                return self::searchTitleMb($title);
            }
            elseif(empty($title)){
                Log::debug("Recherche de l'artiste: ". $artist);
                return self::searchArtistMb($artist);
            }
            else{
                Log::debug("Recherche du titre artiste: ". $title . " " . $artist);
                return self::searchTitleAndArtistMb($title, $artist);
            }
        }

    }

    public static function getWhereArtist($value){
        $mb = new MusicBrainzServiceAgent();
        $recordings= $mb->Recording()->search(sprintf("artist:%s*",$value));
        return self::mapRecordings($recordings);
    }

    private static function mapRecordings($recordings){
        $mappedRecordings = [];
        foreach ($recordings as $item){
            $mappedRecordings[] = Mapper::mapRecording($item);
        }
        return $mappedRecordings;
    }

    public static function saveRecording($recording)
    {
        $table = TableRegistry::get("Recordings");
        $table->save($recording);
    }
}
