<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 18/12/2017
 * Time: 15:06
 */

namespace App\Model\BU;


use App\Model\Entity\Artist;
use Cake\ORM\TableRegistry;
use MusicBrainz\MusicBrainzServiceAgent;

class ArtistManager
{
    public static function getArtistsFromMbWhere($value){

    }

    /**
     * Récupère les artistes pour une liste d'Id donnée
     * @param $array
     * @return array
     */
    public static function getArtistsFromMbWhereIds($array){
        $mb = new MusicBrainzServiceAgent();
        $query = "arid:";
        $query .= join(" OR arid:", $array);
        $artists = $mb->Artist()->search($query);
        return self::mapArtists($artists);
    }

    public static function getArtistFromMbWhereId($id){
        $mb = new MusicBrainzServiceAgent();
        $query = "arid:".$id;
        $artists = $mb->Artist()->search($query);
        return self::mapArtists($artists)[0];
    }

    private static function mapArtists($artists){
        $mappedRecordings = [];
        if(count($artists)==1){
            $mappedRecordings[]=Mapper::mapArtist($artists[0]);
        }
        else{
            foreach ($artists as $item){
                $mappedRecordings[]=Mapper::mapArtist($item);
            }
        }
        return $mappedRecordings;
    }

    public static function saveArtists($array){
        $artistsTable = TableRegistry::get('Artists');
        $artistsTable->saveMany($array);

    }

    public static function saveArtist($artist){
        $artistsTable = TableRegistry::get('Artists');
        $artistsTable->save($artist);
    }

    public static function deleteArtist($artist){
        $artistsTable = TableRegistry::get('Artists');
        $artistsTable->delete($artist);
    }

}
