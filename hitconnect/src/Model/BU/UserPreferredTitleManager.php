<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 18/12/2017
 * Time: 18:25
 */

namespace App\Model\BU;


use App\Model\Entity\UserPreferredTitle;
use Cake\ORM\TableRegistry;

class UserPreferredTitleManager
{
    /**
     * Récupère une liste
     * @param integer $userId User Id
     * @return array of UserPreferredTitles
     */
    public static function getUserPreferredTitlesOfUser($userId){
        $query = TableRegistry::get('UserPreferredTitles')->find();
        $query
            ->where(['user_id' => $userId])
            ->contain(['Recordings.Artists'])
            ->select(['id','Recordings.label', 'Artists.label']);
            return $query->toArray();
    }

    public static function saveUserPreferredTitles($array){
        $table = TableRegistry::get('UserPreferredTitles');
        $table->saveMany($array);
    }

    public static function saveUserPreferredTitle(UserPreferredTitle $upt){
        $table = TableRegistry::get('UserPreferredTitles');
        $table->save($upt);
    }

    public static function removeUserPreferredTitle($id)
    {
        $table = TableRegistry::get('UserPreferredTitles');
        $upt = $table->get($id);
        $table->delete($upt);
    }

}
