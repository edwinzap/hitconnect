<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 26/12/2017
 * Time: 13:01
 */

namespace App\Model\BU;


use App\Model\Entity\Connection;
use Cake\Database\Schema\Table;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Database;
use PDO;



class ConnectionManager
{
    const CONNECTION_STATUS_ACCEPTED = 'accepted';
    const CONNECTION_STATUS_PENDING = 'pending';
    const CONNECTION_STATUS_DELETED = 'deleted';
    const CONNECTION_STATUS_MATCHED = 'matched';

    const ARTIST_MATCH_COEF = 20;
    const RECORDING_MATCH_COEF = 20;
    const TAG_MATCH_COEF = 5;

    public static function getUserContacts($userid){
        return self::getConnections($userid, self::CONNECTION_STATUS_ACCEPTED);
    }

    public static function getMatches($userid){
        self::setMatches($userid);
        return self::getConnections($userid, self::CONNECTION_STATUS_MATCHED);
    }

    public static function getDemands($userid){
        $query = TableRegistry::get('Connections')->find();
        $query
            ->where(['Connections.source_user_id'=>$userid])
            ->andWhere(['ConnectionStatus.label LIKE'=>self::CONNECTION_STATUS_PENDING])
            ->contain(['ConnectionStatus'])
            ->join([
                'table' =>'users',
                'alias' =>'Users',
                'type' => 'INNER',
                'conditions'=> 'Users.id = Connections.target_user_id'
            ])
            ->select(['id','Users.id', 'Users.firstname', 'Users.lastname' ]);
        return $query->toArray();
    }

    public static function getInvitations($userid){
        $query = TableRegistry::get('Connections')->find();
        $query
            ->where(['Connections.target_user_id'=>$userid])
            ->andWhere(['ConnectionStatus.label LIKE'=>self::CONNECTION_STATUS_PENDING])
            ->contain(['ConnectionStatus'])
            ->join([
                'table' =>'users',
                'alias' =>'Users',
                'type' => 'INNER',
                'conditions'=> 'Users.id = Connections.source_user_id'
            ])
            ->select(['id','Users.id', 'Users.firstname', 'Users.lastname' ]);
        return $query->toArray();
    }

    private static function getConnections($userid, $status){
        $query = TableRegistry::get('Connections')->find();
        $query
            ->where(['OR' => [
                ['Connections.source_user_id'=>$userid],
                ['Connections.target_user_id'=>$userid]
            ]])
            ->andWhere(['ConnectionStatus.label LIKE'=>$status, 'Users.id !=' =>$userid])
            ->contain(['ConnectionStatus'])
            ->join([
                'table' =>'users',
                'alias' =>'Users',
                'type' => 'INNER',
                'conditions'=> 'Users.id = Connections.source_user_id OR Users.id = Connections.target_user_id'
            ])
            ->select(['Connections.id','Users.id', 'Users.firstname', 'Users.lastname' ]);
        return $query->toArray();
    }

    private static function setMatches($userid){
        $recordings = self::getRecordingMatches($userid);
        $artists = self::getArtistMatches($userid);
        $tags = self::getTagMatches($userid);

        $array = [];
        foreach ($recordings as $r){
            $id =$r['user_id'];
            if(!key_exists($id, $array)){
                $array[$id] = 0;
            }
            $array[$id] = $array[$id] + $r['recordings_count']* self::RECORDING_MATCH_COEF;
        }

        foreach ($artists as $a){
            $id =$a['user_id'];
            if(!key_exists($id, $array)){
                $array[$id] = 0;
            }
            $array[$id] = $array[$id] + $a['artists_count']* self::ARTIST_MATCH_COEF;
        }

        foreach ($tags as $t){
            $id =$t['user_id'];
            if(!key_exists($id, $array)){
                $array[$id] = 0;
            }
            $array[$id] = $array[$id] + $t['tags_count']* self::TAG_MATCH_COEF;

        }

        $query = TableRegistry::get('Connections')->find();
        foreach ($array as $key=>$value){
            //Si le pourcentage est supérieur à 100, il y a match
            if($value >= 100){
                $query = $query
                    ->where(['AND' => [
                        ['Connections.source_user_id'=>$userid],
                        ['Connections.target_user_id'=>$key]
                    ]])
                    ->orWhere(['AND' => [
                        ['Connections.source_user_id'=>$key],
                        ['Connections.target_user_id'=>$userid]
                    ]])
                    ->contain(['ConnectionStatus'])
                    ->select(['id', 'ConnectionStatus.id','ConnectionStatus.label']);
                $result = $query->toArray();


                //Si l'utilisateur n'a aucune connexion
                if(count($result)==0){
                    $connection = new Connection();
                    $connection->source_user_id = $userid;
                    $connection->target_user_id = $key;
                    $connection->connection_status_id = self::getConnectionStatusId(self::CONNECTION_STATUS_MATCHED);
                    self::saveConnection($connection);
                }
            }
            //TODO: retirer un match s'il devient obsolète.
        }
    }

    private static function getConnectionStatusId($status){
        return TableRegistry::get('ConnectionStatus')->find()
            ->where(['label'=>$status])
            ->select(['id'])
            ->first()['id'];
}

    private static function getRecordingMatches($userid){
        $query = '
        SELECT upt.user_id as user_id, count(r.id) as recordings_count FROM user_preferred_titles as upt
        INNER JOIN recordings as r ON r.id = upt.recording_id
        where upt.user_id != :id AND r.label IN (
            SELECT r.label FROM user_preferred_titles as upt
            INNER JOIN recordings as r ON r.id = upt.recording_id
            WHERE upt.user_id =:id)
        GROUP BY upt.user_id';

        return self::executeQuery($query, ['id' => $userid]);
    }

    private static function getArtistMatches($userid){

        $query = '
        SELECT upt.user_id as user_id, count(a.id) as artists_count FROM user_preferred_titles as upt
        INNER JOIN recordings as r ON r.id = upt.recording_id
        INNER JOIN artists as a ON r.artist_id = a.id
        where upt.user_id != :id AND a.label IN (
            SELECT a.label FROM user_preferred_titles as upt
            INNER JOIN recordings as r ON r.id = upt.recording_id 
            INNER JOIN artists as a ON r.artist_id = a.id
            where upt.user_id = :id)
        GROUP BY upt.user_id';
        return self::executeQuery($query, ['id' => $userid]);
    }

    private static function getTagMatches($userid){
        $query = '
        SELECT upt.user_id as user_id, count(t.id) as tags_count FROM user_preferred_titles as upt
        INNER JOIN recordings as r ON r.id = upt.recording_id
        INNER JOIN artists as a ON r.artist_id = a.id
        INNER JOIN artists_tags as at ON at.artist_id = a.id
        INNER JOIN tags as t ON t.id = at.tag_id
        WHERE upt.user_id != :id AND t.label IN (
            SELECT t.label FROM user_preferred_titles as upt
            INNER JOIN recordings as r ON r.id = upt.recording_id
            INNER JOIN artists as a ON r.artist_id = a.id
            INNER JOIN artists_tags as at ON at.artist_id = a.id
            INNER JOIN tags as t ON t.id = at.tag_id
            WHERE upt.user_id =:id)    
        GROUP BY upt.user_id';
        return self::executeQuery($query, ['id' => $userid]);
    }

    private static function executeQuery($query, array $params){
        $connection = \Cake\Datasource\ConnectionManager::get('default');
        $results = $connection->execute($query, $params)->fetchAll('assoc');
        return $results;
    }

    public static function saveConnection($connection){
        $table = TableRegistry::get('Connections');
        $table->save($connection);
    }

    public static function setConnectionAccepted($connectionId){
        $connection = TableRegistry::get('Connections')->get($connectionId);
        $connection->connection_status_id = self::getConnectionStatusId(self::CONNECTION_STATUS_ACCEPTED);
        self::saveConnection($connection);
    }

    public static function setConnectionRemoved($connectionId){
        $connection = TableRegistry::get('Connections')->get($connectionId);
        $connection->connection_status_id = self::getConnectionStatusId(self::CONNECTION_STATUS_DELETED);
        self::saveConnection($connection);
    }

    public static function setConnectionPending($connectionId, $sourceUserId, $targetUserId){
        $connection = TableRegistry::get('Connections')->get($connectionId);
        $connection->source_user_id = $sourceUserId;
        $connection->target_user_id= $targetUserId;
        $connection->connection_status_id = self::getConnectionStatusId(self::CONNECTION_STATUS_PENDING);
        self::saveConnection($connection);
    }
}
