<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 18/12/2017
 * Time: 16:17
 */

namespace App\Model\BU;


use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;

class UserManager
{
    public static function saveUser(User $user){
        $userTable = TableRegistry::get('Users');
        return $userTable->save($user);
    }

    public static function patchUser(User $user, $data){
        $userTable = TableRegistry::get('Users');
        $user->role=0;
        return $userTable->patchEntity($user, $data);
    }

    public static function getUser($id)
    {
        $userTable = TableRegistry::get('Users');
        return $userTable->get($id);
    }

}
