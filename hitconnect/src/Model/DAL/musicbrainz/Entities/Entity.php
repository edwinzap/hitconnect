<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 22/10/2017
 * Time: 15:51
 */

namespace MusicBrainz\Entities;


class Entity
{

    const AREA = 'area';
    const ARTIST = 'artist';
    const COLLECTION = 'collection';
    const EVENT = 'event';
    const INSTRUMENT = 'instrument';
    const LABEL = 'label';
    const PLACE = 'place';
    const RECORDING = 'recording';
    const RELEASE = 'release';
    const RELEASE_GROUP = 'release-group';
    const SERIES = 'series';
    const WORK = 'work';
    const URL = 'url';
}
