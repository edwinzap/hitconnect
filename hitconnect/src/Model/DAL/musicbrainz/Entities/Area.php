<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 23/10/2017
 * Time: 07:51
 */

namespace MusicBrainz\Entities;


class Area
{
    private $id;
    private $name;
    private $sortName;

    public static function arrayToEntity($array){
        $area = new Area();
        foreach ($array as $key=>$value){
            switch ($key){
                case 'id':
                    $area->setId($value);
                    break;
                case 'name':
                    $area->setName($value);
                    break;
                case 'sort-name':
                    $area->setSortName($value);
                    break;
            }
        }
        return $area;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSortName()
    {
        return $this->sortName;
    }

    /**
     * @param mixed $sortName
     */
    public function setSortName($sortName)
    {
        $this->sortName = $sortName;
    }


}
