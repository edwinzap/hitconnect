<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 16/12/2017
 * Time: 18:38
 */

namespace MusicBrainz\Entities;


class Tag
{
    private $name;

    /**
     * Tag constructor.
     * @param $label
     */
    public function __construct($label=null)
    {
        $this->name = $label;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public static function arrayToEntity($array){
        $tag = new Tag();
        foreach ($array as $key=>$value){
            switch ($key){
                case 'name':
                    $tag->setName($value);
                    break;
                default:
                    break;
            }
        }
        return $tag;
    }


}
