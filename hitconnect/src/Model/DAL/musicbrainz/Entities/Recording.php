<?php

/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 16/10/2017
 * Time: 20:19
 */
namespace MusicBrainz\Entities;

class Recording
{
    private $id;
    private $title;
    private $artist;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param mixed $artist
     */
    public function setArtist(Artist $artist)
    {
        $this->artist = $artist;
    }

    public static function arrayToEntity($array){
        $recording = new Recording();
        foreach ($array as $key=>$value){
            switch ($key){
                case 'id':
                    $recording->setId($value);
                    break;
                case 'title':
                    $recording->setTitle($value);
                    break;
                default:
                    break;
            }
        }
        return $recording;
    }
}
