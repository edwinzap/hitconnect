<?php

/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 16/10/2017
 * Time: 20:11
 */
namespace MusicBrainz\Entities;

class Artist
{
    private $id;
    private $name;
    private $sortName;
    private $tags =[];
    private $type;
    private $gender;


    public static function arrayToEntity($array){

        $artist = new Artist();
        foreach ($array as $key=>$value){
            switch ($key){
                case 'id':
                    $artist->setId($value);
                    break;
                case 'name':
                    $artist->setName($value);
                    break;
                case 'sort-name':
                    $artist->setSortName($value);
                    break;
                case 'type':
                    $artist->setType($value);
                    break;
                case 'gender':
                    $artist->setGender($value);
                    break;
                case 'tags':
                    $artist->setTags(self::processTags($array['tags']));
                   break;
                default:
                    break;
            }
        }
        return $artist;
    }

    private static function processTags($array){
        $tags = [];
        foreach ($array as $item){
            foreach ($item as $key=>$value){
                if($key == 'name'){
                    $tags[]=new Tag($value);
                }
            }
        }
        return $tags;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSortName()
    {
        return $this->sortName;
    }

    /**
     * @param mixed $sortName
     */
    public function setSortName($sortName)
    {
        $this->sortName = $sortName;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

}
