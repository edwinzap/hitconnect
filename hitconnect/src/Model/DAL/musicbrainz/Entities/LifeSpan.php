<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 23/10/2017
 * Time: 08:11
 */

namespace MusicBrainz\Entities;


class LifeSpan
{
    private $end;
    private $begin;
    private $ended;


    public static function arrayToEntity($array){
        $lifeSpan = new LifeSpan();
        foreach ($array as $key=>$value){
            switch ($key){
                case 'end':
                    $lifeSpan->setEnd($value);
                    break;
                case 'begin':
                    $lifeSpan->setBegin($value);
                    break;
                case 'ended':
                    $lifeSpan->setEnded($value);
                    break;
            }
        }
        return $lifeSpan;
    }

    /**
     * @return mixed
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param mixed $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return mixed
     */
    public function getBegin()
    {
        return $this->begin;
    }

    /**
     * @param mixed $begin
     */
    public function setBegin($begin)
    {
        $this->begin = $begin;
    }

    /**
     * @return mixed
     */
    public function getEnded()
    {
        return $this->ended;
    }

    /**
     * @param mixed $ended
     */
    public function setEnded($ended)
    {
        $this->ended = $ended;
    }

}
