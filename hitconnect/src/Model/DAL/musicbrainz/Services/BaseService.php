<?php

/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 20/10/2017
 * Time: 12:49
 */
namespace MusicBrainz\Services;

use Cake\Core\Configure;
use Cake\Log\Log;
use Exception;
use MusicBrainz\CurlHelper;
use MusicBrainz\Entities\Entity;

abstract class BaseService
{
    /*We have 12 resources on our web service which represent core entities in our database:

    area, artist, event, instrument, label, place, recording, release, release-group, series, work, url

    We also provide a web service interface for the following non-core resources:

    rating, tag, collection

    And we allow you to perform lookups based on other unique identifiers with these resources:

    discid, isrc, iswc

    On each entity resource, you can perform three different GET requests:

    lookup:   /<ENTITY>/<MBID>?inc=<INC>
    browse:   /<ENTITY>?<ENTITY>=<MBID>&limit=<LIMIT>&offset=<OFFSET>&inc=<INC>
    search:   /<ENTITY>?query=<QUERY>&limit=<LIMIT>&offset=<OFFSET>*/

    private $entity;
    private $url;
    private $fmt = 'json';

    /**
     * @return mixed
     */
    private function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    private function setUrl($url)
    {
        if ($this->fmt == 'json'){
            $this->url = $url .'&fmt=json';
        }
        else{
            $this->url = $url;
        }
        Log::write('debug', "Url musicbrainz: " . $url);
    }
    /**
     * @param mixed $entity
     */
    protected function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    private function getBaseUrl(){
        Configure::load('musicbrainz');
        return Configure::read('baseUrl');
    }

    private function getSearchUrl(){
        return $this->getEntityUrl($this->entity) . "/?query=";
    }

    private function getEntityUrl(){
        return $this->getBaseUrl() . $this->entity;
    }

    protected function search($query, $limit=null){
        $query=urlencode($query);

        if(isset($limit)){
            $this->setUrl($this->getSearchUrl($this->entity) . $query . "&limit=" . $limit);
        }
        else{
            $this->setUrl($this->getSearchUrl($this->entity) . $query);
        }
        return $this->fetchResponse($this->getUrl());
    }

    protected function lookup($id){
        $this->setUrl(sprintf("%s%s:%s", $this->getSearchUrl(), $this->getEntityId(), $id ));
        return $this->fetchResponse($this->getUrl());
    }

    private function fetchResponse($url){
        $response = null;
        try {
            $response = json_decode(CurlHelper::get($url), true);
        } catch (Exception $e) {
            print $e->getMessage();
        }
        return $response;
    }

    private function getEntityId(){
        switch ($this->getEntity()){
            case Entity::ARTIST:
                return "arid";
            case Entity::RECORDING:
                return "reid";
        }
    }
}
