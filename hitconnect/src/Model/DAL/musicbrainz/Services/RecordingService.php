<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 20/10/2017
 * Time: 13:22
 */

namespace MusicBrainz\Services;


use MusicBrainz\Entities\Artist;
use MusicBrainz\Entities\Entity;
use MusicBrainz\Entities\Recording;

class RecordingService extends BaseService
{
    public function __construct(){
        $this->setEntity(Entity::RECORDING);
    }

    public function getById($id){
        $result = Recording::arrayToEntity(parent::lookup($id));
        return $result;
    }

    public function search($query, $limit=null){
        $array = parent::search($query, $limit);
        return $this->arrayToRecordings($array['recordings']);
    }

    /**
     * Transforme le tableau json en une liste d'objets Recording
     * @param $array array Tableau JSON
     * @return array Liste de Recording
     */
    private function arrayToRecordings($array){
        $recordings = [];
        if($array != null){
            foreach ($array as $item){
                $recording = Recording::arrayToEntity($item);
                $artist = Artist::arrayToEntity($item['artist-credit'][0]['artist']);
                $recording->setArtist($artist);
                $recordings[]=$recording;
            }
        }
        return $recordings;
    }
}
