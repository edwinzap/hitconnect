<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 20/10/2017
 * Time: 13:21
 */

namespace MusicBrainz\Services;

use Cake\Log\Log;
use MusicBrainz\Entities\Artist;
use MusicBrainz\Entities\Entity;

class ArtistService extends BaseService
{
    public function __construct(){
        $this->setEntity(Entity::ARTIST);
    }

    public function getById($id){
        $response = parent::lookup($id);
        if(array_key_exists('artists', $response)){
            if(count($response['artists'])>0){
                return Artist::arrayToEntity($response['artists'][0]);
            }
        }
        Log::write('debug',$response);
        return null;
    }

    public function search($query, $limit=null){
        $array = parent::search($query, $limit);
        return $this->arrayToArtists($array['artists']);
    }

    private function arrayToArtists($array){
        $artists = [];
        foreach ($array as $item){
            $artist = Artist::arrayToEntity($item);
            $artists[]=$artist;
        }
        return $artists;
    }
}
