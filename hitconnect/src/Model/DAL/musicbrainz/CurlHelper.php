<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 20/10/2017
 * Time: 18:02
 */

namespace MusicBrainz;


use Aura\Intl\Exception;

class CurlHelper
{
    static function get($url){
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl, CURLOPT_USERAGENT,"HitConnect");
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);
        $result = curl_exec($curl); /*Retuns result on success, FALSE on failure*/
        if($result == false){
            $error = curl_error($curl);
            curl_close($curl);
            throw new Exception($error);
        }
        else{
            curl_close($curl);
        }
        return $result;
    }

}
