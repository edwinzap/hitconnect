<?php

/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 20/10/2017
 * Time: 12:56
 */
namespace MusicBrainz;

use MusicBrainz\Services\ArtistService;
use MusicBrainz\Services\RecordingService;

class MusicBrainzServiceAgent
{
    function Artist(){
        return new ArtistService();
    }

    function Recording(){
        return new RecordingService();
    }

}
