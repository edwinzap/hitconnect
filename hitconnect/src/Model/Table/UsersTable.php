<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\UserPreferredTitlesTable|\Cake\ORM\Association\HasMany $UserPreferredTitles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('UserPreferredTitles', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 255, 'Ce nom est trop long')
            ->requirePresence('lastname', 'create', 'Un nom est requis')
            ->notEmpty('lastname');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 255, 'Ce prénom est trop long')
            ->requirePresence('firstname', 'create', 'Un prénom est requis')
            ->notEmpty('firstname');

        $validator
            ->date('birthday', 'Date invalide')
            ->allowEmpty('birthday');

        $validator
            ->email('email')
            ->maxLength('email', 255, 'Cet email est trop long')
            ->requirePresence('email', 'create', 'Un email est requis')
            ->notEmpty('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255, 'Ce mot de passe est trop long')
            ->requirePresence('password', 'create', 'Un mot de passe est requis')
            ->notEmpty('password');

        $validator->add('password', [
            'compare' => [
                'rule' => ['compareWith', 'confirm_password'],
                'message' => 'Les mots de passe doivent correspondre'
            ],
        ]);

        $validator
            ->integer('role');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email'], 'Cet email existe déjà'));

        return $rules;
    }
}
