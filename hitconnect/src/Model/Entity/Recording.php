<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Recording Entity
 *
 * @property int $id
 * @property string $mbid
 * @property string $label
 * @property int $artist_id
 *
 * @property \App\Model\Entity\Artist $artist
 * @property \App\Model\Entity\UserPreferredTitle[] $user_preferred_titles
 */
class Recording extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'mbid' => true,
        'label' => true,
        'artist_id' => true,
        'artist' => true,
        'user_preferred_titles' => true
    ];
}
