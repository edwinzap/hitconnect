<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UserPreferredTitle Entity
 *
 * @property int $id
 * @property int $rating
 * @property int $user_id
 * @property int $recording_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Recording $recording
 */
class UserPreferredTitle extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'rating' => true,
        'user_id' => true,
        'recording_id' => true,
        'user' => true,
        'recording' => true
    ];
}
