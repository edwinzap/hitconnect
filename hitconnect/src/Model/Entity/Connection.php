<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Connection Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $source_user_id
 * @property int $target_user_id
 * @property int $connection_status_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ConnectionStatus $connection_status
 */
class Connection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'source_user_id' => true,
        'target_user_id' => true,
        'connection_status_id' => true,
        'user' => true,
        'connection_status' => true
    ];
}
