<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Artist Entity
 *
 * @property int $id
 * @property string $mbid
 * @property string $label
 *
 * @property \App\Model\Entity\Recording[] $recordings
 * @property \App\Model\Entity\Tag[] $tags
 */
class Artist extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'mbid' => true,
        'label' => true,
        'recordings' => true,
        'tags' => true
    ];
}
