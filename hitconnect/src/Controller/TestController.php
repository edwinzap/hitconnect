<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 18/12/2017
 * Time: 16:06
 */

namespace App\Controller;


use App\Model\BU\ArtistManager;
use App\Model\Entity\Recording;
use App\Model\Entity\UserPreferredTitle;
use Cake\Event\Event;

class TestController extends AppController
{
    public function index(){

    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }
}
