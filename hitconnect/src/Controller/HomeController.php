<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 14/10/2017
 * Time: 13:44
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use MusicBrainz\MusicBrainz;


class HomeController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'about']);
    }

    public function index(){
        if($this->Auth->user()){
            $this->redirect('/users/index');
        }
        else{
            $this->set('activeItem','home');
            $this->render();
        }
    }

    public function about(){
        $this->set('activeItem','about');
        $this->render();
    }

}
