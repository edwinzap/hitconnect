<?php
/**
 * Created by IntelliJ IDEA.
 * User: forge
 * Date: 15/10/2017
 * Time: 09:06
 */

namespace App\Controller;

use App\Model\BU\ArtistManager;
use App\Model\BU\ConnectionManager;
use App\Model\BU\RecordingManager;
use App\Model\BU\UserManager;
use App\Model\BU\UserPreferredTitleManager;
use App\Model\Entity\Artist;
use App\Model\Entity\Recording;
use App\Model\Entity\UserPreferredTitle;
use Cake\Event\Event;


class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index(){
        if(count($this->getUpts())<5){
            $this->Flash->error("Ajoutez au moins 5 titres préférés pour finaliser votre profil");
            $this->redirect('/users/preferences');
        }
        else{
            $id = $this->Auth->user()['id'];
            $this->setProfile($id);

            $contacts = ConnectionManager::getUserContacts($id);
            $contacts = $this->setContacts($contacts);
            $this->set('contacts',$contacts);

            $invitations = ConnectionManager::getInvitations($id);
            $invitations = $this->setContacts($invitations);
            $this->set('invitations', $invitations);

            $demands = ConnectionManager::getDemands($id);
            $demands = $this->setContacts($demands);
            $this->set('demands', $demands);

            $matches = ConnectionManager::getMatches($id);
            $matches = $this->setContacts($matches);
            $this->set('matches', $matches);

            $this->set('activeItem','profile');
            $this->render('index');
        }
    }

    private function setContacts($contacts)
    {
        $result = [];

        foreach ($contacts as $contact){
            $user = $contact['Users'];
            $result[]= [
                'connectionid'=>$contact['id'],
                'userid'=>$user['id'],
                'lastname'=> $user['lastname'],
                'firstname'=> $user['firstname']
            ];
        }
        return $result;
    }

    public function acceptContact(){
        $id= $this->request->getData("id");
        ConnectionManager::setConnectionAccepted($id);
        $this->index();
    }

    public function removeContact(){
        $id= $this->request->getData("id");
        ConnectionManager::setConnectionRemoved($id);
        $this->index();
    }

    public function askContact(){
        $id= $this->request->getData("id");
        $sourceUserId = $this->Auth->user()['id'];
        $targetUserId= $this->request->getData("userid");
        ConnectionManager::setConnectionPending($id,$sourceUserId, $targetUserId );
        $this->index();
    }

    public function login(){
        if ($this->request->is('post')) {
            $user = $this->Auth->identify(); // si on a une base de données

            if($user){
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Email ou mot de passe invalide !'));
        }
        $this->set('activeItem','home');
    }

    public function logout(){
        $this->redirect($this->Auth->logout());
    }

    public function add(){
        $user = $this->Users->newEntity();
        if ($this->request->is('post')){
            $user  = UserManager::patchUser($user, $this->request->getData());

            if (UserManager::saveUser($user)){
                $this->Flash->success(__("L'utilisateur a bien été sauvegardé"));
                return $this->redirect(['action'=>'login']);
            }
            else{
                $this->Flash->error(__("Impossible de sauvegarder l'utilisateur"));
            }
        }
        $this->set('user', $user);
        $this->set('activeItem', 'home');
    }

    public function preferences(){
        if ($this->request->is('post')){
            $title = $this->request->getData("search_title");
            $artist = $this->request->getData("search_artist");
            $titles = RecordingManager::searchMb($title, $artist);
            $this->set('titles', $titles);
            /*$this->set('_serialize', ['data']);*/
            $this->render("/Element/titles");
        }
        else{
            $this->getUpts();
        }
        $this->set('activeItem','preferences');
    }

    public function addRecording(){
        $recording = new Recording();
        $recording->mbid = $this->request->getData("recordingid");
        $recording->label = $this->request->getData("recordingname");
        $artist = ArtistManager::getArtistFromMbWhereId($this->request->getData("artistid"));
        $recording->artist = $artist;

        $upt = new UserPreferredTitle();
        $upt->recording = $recording;
        $upt->user_id = $this->Auth->user()['id'];
        $upt->rating = 3;
        RecordingManager::saveRecording($recording);
        UserPreferredTitleManager::saveUserPreferredTitle($upt);

        $this->getUpts();
        $this->render('/Element/upts');
    }

    public function removeRecording(){
        UserPreferredTitleManager::removeUserPreferredTitle($this->request->getData('uptid'));
        $this->getUpts();
        $this->render('/Element/upts');
    }

    private function getUpts(){
        $upts = UserPreferredTitleManager::getUserPreferredTitlesOfUser($this->Auth->user()['id']);
        $this->set('upts', $upts);
        return $upts;
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['signin']);
        $this->Auth->allow(['add']);
    }

    public function view($id){
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function getContact(){
        $id = $this->request->getData('id');
        $this->setProfile($id);
        $this->render('/Element/profile');
    }

    private function setProfile($id){
        $user = UserManager::getUser($id);
        $this->set('user', $user);
        $upts = UserPreferredTitleManager::getUserPreferredTitlesOfUser($id);
        $this->set('upts', $upts );
    }
}

//TODO: rating des titres.
//TODO: vérifier l'existence d'un artist/recording/tag avant d'ajouter
//TODO: améliorer l'algorithme de match
